package com.ruchia.ruchia.model;

public class ReqResponse {
    private Boolean isError;
    private String errorMessage;
    private Object data;

    public ReqResponse() {
    }

    public ReqResponse(Boolean isError, Object data, String errorMessage) {
        this.isError = isError;
        this.errorMessage = errorMessage;
        this.data = data;
    }

    public Boolean getIsError() {
        return isError;
    }

    public void setIsError(Boolean error) {
        isError = error;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String error) {
        errorMessage = error;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
