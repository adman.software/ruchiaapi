package com.ruchia.ruchia.model;

import java.util.HashMap;
import java.util.Map;

public class Rucher {

    private String id;
    private String nom;
    private String adresse;
    private String description;
    private Map<String, String> ruches;
    private String apiculteurId;

    public Rucher() {
        this.ruches = new HashMap<String, String>();
    }

    // getters and setters

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Map<String, String> getRuches() {
        return ruches;
    }

    public void setRuches(Map<String, String> ruches) {
        this.ruches = ruches;
    }

    public String getApiculteurId() {
        return apiculteurId;
    }

    public void setApiculteurId(String apiculteurId) {
        this.apiculteurId = apiculteurId;
    }
}
