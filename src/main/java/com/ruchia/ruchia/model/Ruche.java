package com.ruchia.ruchia.model;

public class Ruche {
    private String id;
    private String nom;
    private String apiculteurId;
    private boolean alerte_ouverture;
    private boolean couvercle_ferme;
    private String humidite;
    private String temperature;

    public Ruche() {
        
    }

    public String getId() {
        return id;
    }

    public void setId(String rucheId) {
        this.id = rucheId;

        if (this.nom == null)
            this.setNom(rucheId);
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String rucheNom) {
        this.nom = rucheNom;
    }

    public String getApiculteurId() {
        return apiculteurId;
    }

    public void setApiculteurId(String apiculteurId) {
        this.apiculteurId = apiculteurId;
    }

    public boolean getAlerte_ouverture() {
        return alerte_ouverture;
    }

    public void setAlerte_ouverture(boolean alerte_ouverture) {
        this.alerte_ouverture = alerte_ouverture;
    }

    public boolean getCouvercle_ferme() {
        return couvercle_ferme;
    }

    public void setCouvercle_ferme(boolean couvercle_ferme) {
        this.couvercle_ferme = couvercle_ferme;
    }

    public String getHumidite() {
        return humidite;
    }

    public void setHumidite(String humidite) {
        this.humidite = humidite;
    }

    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

}
