package com.ruchia.ruchia.controller;

import com.ruchia.ruchia.model.ReqResponse;
import com.ruchia.ruchia.model.User;
import com.ruchia.ruchia.service.AuthService;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/auth")
public class AuthController {
    private AuthService authService;

    public AuthController(AuthService authService) {
        this.authService = authService;
    }

    @PostMapping
    public ReqResponse authenticate(@RequestBody User user) throws Exception {
        try {
            String token = this.authService.authUser(user.getEmail(), user.getMot_de_passe());

            ReqResponse response = new ReqResponse(false, token, null);
            return response;
        }
        catch (Exception e) { 
            ReqResponse response = new ReqResponse(true, null, e.getMessage());
            return response;
        }
    }
}
