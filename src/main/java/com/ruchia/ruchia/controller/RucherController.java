package com.ruchia.ruchia.controller;

import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ruchia.ruchia.model.ReqResponse;
import com.ruchia.ruchia.model.Rucher;
import com.ruchia.ruchia.service.RucherService;

@RestController
@RequestMapping("/api/rucher")
public class RucherController {
    private RucherService rucherService;

    public RucherController(RucherService rucherService) {
        this.rucherService = rucherService;
    }

    @GetMapping()
    public ReqResponse getAll() {
        try {
            List<Rucher> ruchers = this.rucherService.getAll();
            return new ReqResponse(false, ruchers, null);
        } catch (Exception e) {
            return new ReqResponse(true, null, e.getMessage());
        }
    }

    @PostMapping()
    public ReqResponse createRucher(@RequestBody Rucher rucher) {
        try {
            Rucher createdRucher = this.rucherService.createRucher(rucher.getNom(), rucher.getAdresse(), rucher.getDescription());
            return new ReqResponse(false, null, null);
        } catch (Exception e) {
            return new ReqResponse(true, null, e.getMessage());
        }
    }

    @PutMapping("/{rucherId}")
    public ReqResponse updateRucher(@PathVariable("rucherId") String rucherId, @RequestBody Rucher rucher) {
        try {
            Rucher updatedRucher = this.rucherService.updateRucher(rucherId, rucher.getNom(), rucher.getAdresse(), rucher.getDescription());
            return new ReqResponse(false, updatedRucher, null);
        } catch (Exception e) {
            return new ReqResponse(true, null, e.getMessage());
        }
    }

    @DeleteMapping("/{rucherId}")
    public ReqResponse deleteRucher(@PathVariable("rucherId") String rucherId) {
        try {
            this.rucherService.deleteRucher(rucherId);
            return new ReqResponse(false, null, null);
        } catch (Exception e) {
            return new ReqResponse(true, null, e.getMessage());
        }
    }
    
    @PutMapping("/addruche/{rucherId}/{hiveId}")
    public ReqResponse addHiveToRucher(@PathVariable("rucherId") String rucherId, @PathVariable("hiveId") String hiveId) {
        try {
            Rucher rucher = this.rucherService.getById(rucherId);
            if (rucher == null) {
                return new ReqResponse(true, null, "Rucher not found");
            }

            this.rucherService.addHiveToRucher(rucherId, hiveId);
            return new ReqResponse(false, null, null);
        } catch (Exception e) {
            return new ReqResponse(true, null, e.getMessage());
        }
    }

    @DeleteMapping("/removeruche/{rucherId}/{hiveId}")
    public ReqResponse removeHiveFromRucher(@PathVariable("rucherId") String rucherId, @PathVariable("hiveId") String hiveId) {
        try {
            this.rucherService.removeHiveFromRucher(rucherId, hiveId);
            return new ReqResponse(false, null, null);
        } catch (Exception e) {
            return new ReqResponse(true, null, e.getMessage());
        }
    }
}
