package com.ruchia.ruchia.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.UserRecord;
import com.google.firebase.auth.UserRecord.CreateRequest;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.ruchia.ruchia.model.ReqResponse;
import com.ruchia.ruchia.model.User;

@RestController
@RequestMapping("/api/user")
public class UserController {
    private final FirebaseDatabase firebaseDatabase;

    public UserController(FirebaseDatabase firebaseDatabase) {
        this.firebaseDatabase = firebaseDatabase;
    }

    @PostMapping("/")
    public ReqResponse addUser(@RequestBody User user) {
        try {
            CreateRequest request = new CreateRequest()
                .setEmail(user.getEmail())
                .setEmailVerified(false)
                .setPassword(user.getMot_de_passe())
                .setDisabled(false);

            UserRecord userRecord = FirebaseAuth.getInstance().createUser(request);
            System.out.println("Successfully created new user: " + userRecord.getUid());

            // Set the id of the user
            user.setId(userRecord.getUid());

            DatabaseReference ref = firebaseDatabase.getReference("apiculteur/" + userRecord.getUid());
            ref.setValueAsync(user);

            return new ReqResponse(false, userRecord.getUid(), null);
        } catch (Exception e) {
            return new ReqResponse(true, null, e.getMessage());
        }
    }
}
