package com.ruchia.ruchia.controller;

import com.ruchia.ruchia.model.ReqResponse;
import com.ruchia.ruchia.model.Ruche;
import com.ruchia.ruchia.service.RucheService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.ExecutionException;

@RestController
@RequestMapping("/api/ruche")
public class RucheController {

    private final RucheService rucheService;

    public RucheController(RucheService rucheService) {
        this.rucheService = rucheService;
    }
    
    @GetMapping()
    public ReqResponse getAll() {
        try {
            return new ReqResponse(false, rucheService.getAll(), null);
        } catch (Exception e) {
            return new ReqResponse(true, null, e.getMessage());
        }
    }

    @GetMapping("/{hiveId}")
    public ReqResponse getHive(@PathVariable String hiveId) throws ExecutionException, InterruptedException {
        try {
            Ruche ruche = rucheService.getById(hiveId);
            if (ruche == null) {
                return new ReqResponse(true, null, "Ruche not found");
            }
            
            return new ReqResponse(false, ruche, null);
        } catch (Exception e) {
            return new ReqResponse(true, null, e.getMessage());
        }
    }

    @PutMapping("/{hiveId}")
    public ReqResponse updateHive(@PathVariable String hiveId, @RequestBody Ruche ruche) {
        try {
            this.rucheService.update(hiveId, ruche);            
            return new ReqResponse(false, null, null);
        } catch (Exception e) {
            return new ReqResponse(true, null, e.getMessage());
        }
    }
}
