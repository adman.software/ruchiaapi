package com.ruchia.ruchia;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RuchiaApplication {

	public static void main(String[] args) {
		SpringApplication.run(RuchiaApplication.class, args);
	}

}
