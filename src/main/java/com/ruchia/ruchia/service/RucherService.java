package com.ruchia.ruchia.service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.google.firebase.auth.FirebaseToken;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.ruchia.ruchia.model.Ruche;
import com.ruchia.ruchia.model.Rucher;

@Service
public class RucherService {
    private DatabaseReference databaseReference;
    private RucheService rucheService;

    public RucherService(FirebaseDatabase firebaseDatabase, RucheService rucheService) {
        this.databaseReference = FirebaseDatabase.getInstance().getReference();
        this.rucheService = rucheService;
    }

    public List<Rucher> getAll () throws InterruptedException, ExecutionException {
        DatabaseReference ref = this.databaseReference.child("rucher");

        CompletableFuture<List<Rucher>> completableFuture = new CompletableFuture<>();
        ValueEventListener listener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<Rucher> ruchers = new ArrayList<Rucher>();
                for (DataSnapshot rucherSnapshot : dataSnapshot.getChildren()) {
                    Rucher rucher = rucherSnapshot.getValue(Rucher.class);
                    ruchers.add(rucher);
                }
                completableFuture.complete(ruchers);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                completableFuture.completeExceptionally(databaseError.toException());
            }
        };

        ref.addListenerForSingleValueEvent(listener);
        List<Rucher> ruchers = completableFuture.get();
        return ruchers;
    }

    public Rucher getById(String id) throws InterruptedException, ExecutionException {
        DatabaseReference ref = this.databaseReference.child("rucher/" + id);


        CompletableFuture<Rucher> completableFuture = new CompletableFuture<>();
        ValueEventListener listener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Rucher rucher = dataSnapshot.getValue(Rucher.class);
                completableFuture.complete(rucher);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                completableFuture.completeExceptionally(databaseError.toException());
            }
        };

        ref.addListenerForSingleValueEvent(listener);
        Rucher rucher = completableFuture.get();
        return rucher;
    }

    public Rucher createRucher(String name, String adress, String description) {
        Rucher rucher = new Rucher();
        rucher.setNom(name);
        rucher.setAdresse(adress);
        rucher.setDescription(description);

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        FirebaseToken firebaseToken = (FirebaseToken) authentication.getPrincipal();
        
        DatabaseReference rucherRef = databaseReference.child("rucher").push();
        String id = rucherRef.getKey();
        rucher.setId(id);
        rucher.setApiculteurId(firebaseToken.getUid());
        rucherRef.setValueAsync(rucher);

        return rucher;
    }

    public Rucher updateRucher(String id, String name, String adress, String description) throws Exception {
        Rucher rucher = getById(id);
        if (rucher == null) {
            throw new Exception("Rucher not found");
        }

        rucher.setNom(name);
        rucher.setAdresse(adress);
        rucher.setDescription(description);

        DatabaseReference rucherRef = databaseReference.child("rucher").child(id);
        rucherRef.setValueAsync(rucher);

        return rucher;
    }

    public void deleteRucher(String id) throws Exception {
        Rucher rucher = getById(id);
        if (rucher == null) {
            throw new Exception("Rucher not found");
        }

        DatabaseReference rucherRef = databaseReference.child("rucher").child(id);
        rucherRef.removeValueAsync();
    }

    public void addHiveToRucher(String rucherId, String hiveId) throws Exception {
        Ruche ruche = rucheService.getById(hiveId);
        if (ruche == null) {
            throw new Exception("Ruche not found");
        }

        DatabaseReference ruchesRef = databaseReference.child("rucher").child(rucherId).child("ruches").child(hiveId);
        ruchesRef.setValueAsync(hiveId);
    }

    public void removeHiveFromRucher(String rucherId, String hiveId) {
        DatabaseReference ruchesRef = databaseReference.child("rucher").child(rucherId).child("ruches").child(hiveId);
        ruchesRef.removeValueAsync();
    }
}
