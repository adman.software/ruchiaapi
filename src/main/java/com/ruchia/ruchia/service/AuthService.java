package com.ruchia.ruchia.service;

import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.springframework.stereotype.Service;

import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;

@Service()
public class AuthService {
    private static final String BASE_URL = "https://www.googleapis.com/identitytoolkit/v3/relyingparty/";
    private static final String OPERATION_AUTH = "verifyPassword";

    private final String firebaseKey = "AIzaSyD4EETpJn3RcrGzTskkKUESFdr4l8RJFWM";
    
    public String authUser (String email, String password) throws ClientProtocolException, IOException, Exception {
        HttpClient httpclient = HttpClients.createDefault();
        HttpPost httppost = new HttpPost(BASE_URL + OPERATION_AUTH + "?key=" + firebaseKey);
        List<NameValuePair> params = new ArrayList<NameValuePair>(1);
        params.add(new BasicNameValuePair("email", email));
        params.add(new BasicNameValuePair("password", password));
        params.add(new BasicNameValuePair("returnSecureToken", "true"));
        httppost.setEntity(new UrlEncodedFormEntity(params, StandardCharsets.UTF_8));
        HttpEntity entity = httpclient.execute(httppost).getEntity();

        JsonParser jp = new JsonParser();
        JsonElement root = jp.parse(new InputStreamReader(entity.getContent()));
        JsonObject rootObj = root.getAsJsonObject();

        if (rootObj.get("error") != null) {
            throw new Exception(rootObj.get("error").getAsJsonObject().get("message").getAsString());
        }
        String token = rootObj.get("idToken").getAsString();
        return token;
    }

}
