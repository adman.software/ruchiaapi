package com.ruchia.ruchia.service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import org.springframework.stereotype.Service;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.ruchia.ruchia.model.Ruche;

@Service
public class RucheService {
    private final FirebaseDatabase firebaseDatabase;

    public RucheService(FirebaseDatabase firebaseDatabase) {
        this.firebaseDatabase = firebaseDatabase;
    }

    public List<Ruche> getAll() throws InterruptedException, ExecutionException {
        DatabaseReference ref = firebaseDatabase.getReference("ruche");

        CompletableFuture<List<Ruche>> completableFuture = new CompletableFuture<>();
        ValueEventListener listener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<Ruche> hives = new ArrayList<Ruche>();
                for (DataSnapshot hiveSnapshot : dataSnapshot.getChildren()) {
                    Ruche hive = hiveSnapshot.getValue(Ruche.class);
                    hives.add(hive);
                }
                completableFuture.complete(hives);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                completableFuture.completeExceptionally(databaseError.toException());
            }
        };

        ref.addListenerForSingleValueEvent(listener);
        List<Ruche> hives = completableFuture.get();
        return hives;
    }

    public Ruche getById(String id) throws InterruptedException, ExecutionException {
        DatabaseReference ref = firebaseDatabase.getReference("ruche/" + id);

        CompletableFuture<Ruche> completableFuture = new CompletableFuture<>();
        ValueEventListener listener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Ruche hive = dataSnapshot.getValue(Ruche.class);
                completableFuture.complete(hive);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                completableFuture.completeExceptionally(databaseError.toException());
            }
        };

        ref.addListenerForSingleValueEvent(listener);
        Ruche ruche = completableFuture.get();
        return ruche;
    }

    public void update(String id, Ruche newRuche) throws Exception {
        Ruche ruche = getById(id);
        if (ruche == null) {
            throw new Exception("Ruche not found");
        }

        ruche.setNom(newRuche.getNom());
        ruche.setAlerte_ouverture(newRuche.getAlerte_ouverture());

        DatabaseReference ref = firebaseDatabase.getReference("ruche/" + id);
        ref.setValueAsync(ruche);
    }
    
}
