package com.ruchia.ruchia;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.database.FirebaseDatabase;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.ResourceLoader;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

@Configuration
public class FirebaseConfig {

    private final ResourceLoader resourceLoader;

    @Autowired
    public FirebaseConfig(ResourceLoader resourceLoader) {
        this.resourceLoader = resourceLoader;
    }

    @Bean
    public FirebaseDatabase firebaseDatabase() throws IOException {
        ClassPathResource serviceAccountResource = new ClassPathResource("firebase-adminsdk.json");
        InputStream serviceAccount = serviceAccountResource.getInputStream();

        FirebaseOptions options = new FirebaseOptions.Builder()
                .setCredentials(GoogleCredentials.fromStream(serviceAccount))
                .setDatabaseUrl("https://ruchia-42e01-default-rtdb.europe-west1.firebasedatabase.app/")
                .build();

        if (FirebaseApp.getApps().isEmpty()) { // Vérifie si une instance de FirebaseApp existe déjà
            FirebaseApp.initializeApp(options);
        }

        return FirebaseDatabase.getInstance();
    }
}
