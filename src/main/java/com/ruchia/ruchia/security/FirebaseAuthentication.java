package com.ruchia.ruchia.security;

import java.util.ArrayList;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import com.google.firebase.auth.FirebaseToken;

public class FirebaseAuthentication extends UsernamePasswordAuthenticationToken {
    private static final long serialVersionUID = 1L;
    private final FirebaseToken decodedToken;

    public FirebaseAuthentication(FirebaseToken decodedToken) {
        super(decodedToken, null, new ArrayList<>());
        this.decodedToken = decodedToken;
    }

    public FirebaseToken getDecodedToken() {
        return decodedToken;
    }
}
