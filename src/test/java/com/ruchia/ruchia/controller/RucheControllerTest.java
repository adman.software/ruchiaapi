package com.ruchia.ruchia.controller;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

import com.fasterxml.jackson.databind.ObjectMapper;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.ruchia.ruchia.model.ReqResponse;
import com.ruchia.ruchia.model.Ruche;
import com.ruchia.ruchia.security.JwtAuthenticationFilterMock;
import com.ruchia.ruchia.service.AuthService;
import com.ruchia.ruchia.service.RucheService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class RucheControllerTest {

    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Mock
    private AuthService authService;

    @Mock
    private RucheService rucheService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);

        RucheController rucheController = new RucheController(rucheService);
        mockMvc = MockMvcBuilders.standaloneSetup(rucheController)
                .addFilter(new JwtAuthenticationFilterMock(), "/api/ruche")
                .build();
    }

    @Test
    public void testGetAll() throws Exception {
        // Créer un objet DatabaseReference mock
        DatabaseReference ref = Mockito.mock(DatabaseReference.class);

        // Créer une liste de ruches de test
        List<Ruche> expectedRuches = new ArrayList<>();
        expectedRuches.add(new Ruche() {{
            setId("ruche1");
            setNom("ruche1");
        }});
        expectedRuches.add(new Ruche() {{
            setId("ruche2");
            setNom("ruche2");
        }});

        // Définir le comportement du mock DatabaseReference
        doAnswer(new Answer<Void>() {
            public Void answer(InvocationOnMock invocation) {
                ValueEventListener listener = invocation.getArgument(0);
                listener.onDataChange(createDataSnapshot(expectedRuches));
                return null;
            }
        }).when(ref).addListenerForSingleValueEvent(any(ValueEventListener.class));
                
        // Perform the request
        MockHttpServletResponse response = mockMvc.perform(MockMvcRequestBuilders
        .get("/api/ruche")
        .accept(MediaType.APPLICATION_JSON))
        .andReturn()
        .getResponse();


        // Verify the response
        assertEquals(HttpStatus.OK.value(), response.getStatus());

        ReqResponse responseData = asObject(response.getContentAsString(), ReqResponse.class);
        assertEquals(false, responseData.getIsError());
        assertEquals(null, responseData.getErrorMessage());
    }

     @Test
    public void testGetHive() throws Exception {
        // Données de test
        String hiveId = "hive123";
        Ruche ruche = new Ruche();
        ruche.setId(hiveId);
        ruche.setNom("Ruche 1");
        // Définir le comportement du service rucheService.getById
        when(rucheService.getById(eq(hiveId))).thenReturn(ruche);

        // Exécuter la requête
        MvcResult result = mockMvc.perform(get("/api/ruche/" + hiveId))
                .andExpect(status().isOk())
                .andReturn();

        // Analyser la réponse JSON
        String responseBody = result.getResponse().getContentAsString();
        ReqResponse response = objectMapper.readValue(responseBody, ReqResponse.class);

        // Vérifier le résultat
        assertFalse(response.getIsError());
        assertEquals(ruche, response.getData());
        assertNull(response.getErrorMessage());
    }

    private DataSnapshot createDataSnapshot(List<Ruche> ruches) {
        DataSnapshot dataSnapshot = Mockito.mock(DataSnapshot.class);
        when(dataSnapshot.getChildren()).thenAnswer(invocation -> ruches.stream().map(this::createDataSnapshot).iterator());
        when(dataSnapshot.getValue(Ruche.class)).thenAnswer(invocation -> {
            DataSnapshot childSnapshot = invocation.getArgument(0);
            return ruches.stream()
                    .filter(ruche -> ruche.getId().equals(childSnapshot.getKey()))
                    .findFirst()
                    .orElse(null);
        });
        return dataSnapshot;
    }    

    private DataSnapshot createDataSnapshot(Ruche ruche) {
        DataSnapshot dataSnapshot = Mockito.mock(DataSnapshot.class);
        when(dataSnapshot.getKey()).thenReturn(ruche.getId());
        return dataSnapshot;
    }

    // Utility method to convert JSON string to object
    private <T> T asObject(String json, Class<T> type) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(json, type);
    }
}
