package com.ruchia.ruchia.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ruchia.ruchia.model.ReqResponse;
import com.ruchia.ruchia.model.User;
import com.ruchia.ruchia.security.JwtAuthenticationFilterMock;
import com.ruchia.ruchia.service.AuthService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

public class AuthControllerTest {

    private MockMvc mockMvc;

    @Mock
    private AuthService authService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);

        AuthController authController = new AuthController(authService);
        mockMvc = MockMvcBuilders.standaloneSetup(authController)
                .addFilter(new JwtAuthenticationFilterMock(), "/api/auth")
                .build();
    }

    @Test
    public void testAuthenticate() throws Exception {
        // Mock the authentication service
        String token = "test-token";
        when(authService.authUser(anyString(), anyString())).thenReturn(token);

        // Prepare the user data
        User user = new User();
        user.setEmail("test@example.com");
        user.setMot_de_passe("password");

        // Perform the request
        MockHttpServletResponse response = mockMvc.perform(MockMvcRequestBuilders
        .post("/api/auth")
        .contentType(MediaType.APPLICATION_JSON)
        .content(asJsonString(user)))
        .andReturn().getResponse();


        // Verify the response
        assertEquals(HttpStatus.OK.value(), response.getStatus());

        ReqResponse responseData = asObject(response.getContentAsString(), ReqResponse.class);
        assertEquals(false, responseData.getIsError());
        assertEquals(token, responseData.getData());
        assertEquals(null, responseData.getErrorMessage());

        // Verify that the authentication service was called with the correct user data
        verify(authService).authUser("test@example.com", "password");
    }

    // Utility method to convert an object to JSON string
    private String asJsonString(Object obj) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(obj);
    }

    // Utility method to convert JSON string to object
    private <T> T asObject(String json, Class<T> type) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(json, type);
    }
}
